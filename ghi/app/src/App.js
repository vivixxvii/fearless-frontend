import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendeeForm from './AttendeeForm';
import MainPage from './MainPage'
import { BrowserRouter, Route,  Routes} from 'react-router-dom'
// import {Router, Route} from 'react-dom'

 
function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
        {/* <AttendeeForm />
    <AttendeesList attendees={props.attendees} />
    <LocationForm />
    <ConferenceForm />
    <PresentationForm /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;


